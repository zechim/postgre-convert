<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'functions.php';

define('YML_PATH', __DIR__ . '/YML/');
define('ENTITY_PATH', __DIR__ . '/ENTITY/');

resetTree(YML_PATH);
resetTree(ENTITY_PATH);

$entityNamespace = 'Urbem\CoreBundle\Entity\\';
$remove = [
    'administracao' => [
        'auditoria_detalhe',
        'auditoria_tabelas_temporarias'
    ],
    'contabilidade' => [
        'consistencia_2',
        'consistencia_3',
        'consistencia_4',
        'consistencia_5',
        'consistencia_6',
        'consistencia_7',
        'consistencia_8',
        'consistencia_9',
        'consistencia_10',
        'consistencia_11',
    ],
    'public' => [
        'cod_atributo',
        'spatial_ref_sys',
        'geometry_columns',
        'temp_almoxarifado_lancamento_bem',
        'temp_bem_atributo_especie',
        'temp_bem_baixado',
        'temp_bem_comprado',
        'temp_bem_comprado_empenho',
        'temp_bem_responsavel',
        'temp_delete_lancamentos',
        'temp_docs',
        'temp_doc',
        'temp_frota_proprio',
        'temp_historico_bem',
        'temp_manutencao',
        'temp_manutencao_paga',
        'temp_par',
        'temp_transparencia_remuneracao',
        'tmp_cpf_controle_dependentes',
        'tmp_valores_decimo',
        'temp_apolice_bem',
        'sw_vw_ultimo_andamento'
    ],
    'imobiliario' => [
        'vw_max_area_un_dep',
    ],
    'empenho' => [
        'sw_vw_ultimo_andamento'
    ]
];

foreach (getTables($pdo = new PDO("pgsql:dbname=urbem_mg;host=127.0.0.1", 'jzechim', 'jzechim'), $entityNamespace, $remove) as $table) {
    $entity = toYAML($pdo, $table, $entityNamespace);

    if (null === $entity) {
        continue;
    }

    $name = $entityPath = '';

    if ('' !== $table['schema']) {
        $name = camelize($table['schema']) . '.';

        $entityPath = camelize($table['schema']) . '/';
    }

    file_put_contents(YML_PATH . $name . camelize($table['table']) . '.orm.yml', $entity[1]);

    if (false === is_dir($entityPath = ENTITY_PATH . $entityPath)) {
        mkdir($entityPath, 0777, true);
    }

    file_put_contents($entityPath . camelize($table['table']) . '.php', toEntity($entity[0], $table, $entityNamespace));
}