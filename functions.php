<?php

require_once __DIR__.'/vendor/autoload.php';

function fixFkName(array $tables, $relationType)
{
    foreach ($tables as $key => $table) {
        if (0 === count($table['foreignKeys'][$relationType])) {
            continue;
        }

        $used = [];

        $append = [];

        foreach ($table['foreignKeys'][$relationType] as $relationKey => $relation) {
            $property = $relation['property'];

            if (true === array_key_exists($property, $used)) {
                $relation['property'] = sprintf('%s%s', $property, $used[$property]);

                $relation['inversedBy'] = sprintf('%s%s', $relation['inversedBy'], $used[$property]);

                $fkName = sprintf('%s_%s', $relationKey, $used[$property]);

                $append[$fkName] = $relation;

                unset($tables[$key]['foreignKeys'][$relationType][$relationKey]);
            }

            if (false === array_key_exists($property, $used)) {
                $used[$property] = 0;
            }

            $used[$property] += 1;
        }

        $tables[$key]['foreignKeys'][$relationType]+= $append;
    }

    return $tables;
}

function fixColumnOrder(array $tables, $relationType)
{
    foreach ($tables as $key => &$table) {
        foreach ($table['foreignKeys'][$relationType] as &$relation) {
            $key = $relation['inversedByKey'];

            if (false === array_key_exists($key, $tables)) {
                continue;
            }

            $targetColumsOrder = [];

            array_walk($tables[$key]['columns'], function($id) use (&$targetColumsOrder) {
                $targetColumsOrder[$id['name']] = count($targetColumsOrder);
            });

            $targetColumns = $sourceColumns = [];

            foreach ($relation['target_columns'] as $index => $column) {
                $targetColumns[$targetColumsOrder[$column]] = $column;
                $sourceColumns[$targetColumsOrder[$column]] = $relation['source_columns'][$index];
            }

            ksort($targetColumns);
            ksort($sourceColumns);

            $relation['target_columns'] = $targetColumns;
            $relation['source_columns'] = $sourceColumns;
        }
    }

    return $tables;
}

function createOneToMany(array $tables)
{
    foreach ($tables as $key => &$table) {
        foreach ($table['foreignKeys']['manyToOne'] as $manyToOne) {
            $key = $manyToOne['inversedByKey'];

            if (false === array_key_exists($key, $tables)) {
                continue;
            }

            $oneToMany = [];
            $oneToMany['property'] = $manyToOne['inversedBy'];
            $oneToMany['target'] = $manyToOne['source'];
            $oneToMany['mappedBy'] = $manyToOne['property'];

            $tables[$key]['foreignKeys']['oneToMany'][] = $oneToMany;
        }
    }

    return $tables;
}

function createOneToOneOwningSide(array $tables)
{
    foreach ($tables as $key => &$table) {
        foreach ($table['foreignKeys']['oneToOneInverseSide'] as &$oneToOneInverseSide) {
            $key = $oneToOneInverseSide['inversedByKey'];

            if (false === array_key_exists($key, $tables)) {
                continue;
            }

            $oneToOneOwningSide = [];
            $oneToOneOwningSide['property'] = $oneToOneInverseSide['inversedBy'];
            $oneToOneOwningSide['target'] = $oneToOneInverseSide['source'];
            $oneToOneOwningSide['inversedBy'] = $oneToOneInverseSide['property'];
            $oneToOneOwningSide['target_columns'] = $oneToOneInverseSide['target_columns'];
            $oneToOneOwningSide['source_columns'] = $oneToOneInverseSide['source_columns'];

            unset($oneToOneInverseSide['source']);

            $oneToOneInverseSide['mappedBy'] = $oneToOneInverseSide['inversedBy'];

            unset($oneToOneInverseSide['inversedBy']);

            $tables[$key]['foreignKeys']['oneToOneOwningSide'][] = $oneToOneOwningSide;
        }
    }

    return $tables;
}

function createOneToOneAndManyToOne(array $tables, $entityNamespace)
{
    $tables = fixColumnOrder($tables, 'relation');
    $tables = fixFkName($tables, 'relation');

    foreach ($tables as $key => $table) {
        foreach ($table['foreignKeys']['relation'] as $fkName => $relation) {
            $sourceColumns = $relation['source_columns'];

            $relationType = 'manyToOne';

            foreach ($table['indexes'] as $sourceIndexes) {
                $diff = array_merge(array_diff($sourceColumns, $sourceIndexes['columns']), array_diff($sourceIndexes['columns'], $sourceColumns));

                // OneToOne
                if (count($diff) === 0 && true === $sourceIndexes['unique']) {
                    $relationType = 'oneToOneInverseSide';

                    $relation['inversedBy'] = 'fk' . str_replace([$entityNamespace, '\\'], '', $relation['source']);
                    break;
                }
            }

            $tables[$key]['foreignKeys'][$relationType][$fkName] = $relation;
        }

        unset($tables[$key]['foreignKeys']['relation']);
    }

    $tables = createOneToMany($tables);
    $tables = createOneToOneOwningSide($tables);

    return $tables;
}

function getDateTimeObject($type)
{
    switch ($type) {
        case 'datetimepk':
            $object = '\Urbem\CoreBundle\Helper\DateTimePK';
            break;

        case 'datetimemicrosecondpk':
            $object = '\Urbem\CoreBundle\Helper\DateTimeMicrosecondPK';
            break;

        case 'datetimetzpk':
            $object = '\Urbem\CoreBundle\Helper\DateTimeTZPK';
            break;

        case 'datepk':
            $object = '\Urbem\CoreBundle\Helper\DatePK';
            break;

        case 'timepk':
            $object = '\Urbem\CoreBundle\Helper\TimePK';
            break;

        case 'timemicrosecondpk':
            $object = '\Urbem\CoreBundle\Helper\TimeMicrosecondPKType';
            break;

        default:
            $object = '\DateTime';
            break;
    }

    return $object;
}

function convertPHPType($type)
{
    $types = [
        'datetimetz'            => '\DateTime',
        'datetimetzpk'          => '\DateTime',
        'datetime'              => '\DateTime',
        'datetimepk'            => '\DateTime',
        'datetimemicrosecondpk' => '\DateTime',
        'date'                  => '\DateTime',
        'datepk'                => '\DateTime',
        'time'                  => '\DateTime',
        'timepk'                => '\DateTime',
        'timemicrosecondpk'     => '\DateTime',
        'bigint'                => 'integer',
        'smallint'              => 'integer',
        'text'                  => 'string',
        'blob'                  => 'string',
        'decimal'               => 'integer',
        'json_array'            => 'array',
        'simple_array'          => 'array',
        'float'                 => 'integer',
        'integer'               => 'integer',
        'string'                => 'string',
        'boolean'               => 'boolean',
    ];

    return $types[$type];
}

function convertDoctrineType(PDO $pdo, $type, $column, $table, $id = false)
{
    switch (true) {
        case $type == 'datetimetz' && true == $id:
            $type = 'datetimetzpk';
            break;

        case $type == 'date' && true == $id:
            $type = 'datepk';
            break;

        case $type == 'datetime' || $type == 'time':
            $length = $type === 'datetime' ? 19 : 8;

            $stmt = $pdo->prepare(sprintf('SELECT COUNT(*) FROM %s WHERE LENGTH(CAST(%s AS text)) > %s', $table, $column, $length));
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $result = (int) reset($result[0]);

            // with microseconds
            if (0 < $result) {
                $type = $type == 'datetime' ? 'datetimemicrosecondpk' : 'timemicrosecondpk';

            } elseif (true === $id) {
                $type = $type == 'datetime' ? 'datetimepk' : 'timepk';
            }

            break;
    }

    return $type;
}

function buildSetterAndGetter($entityName, $property, $options, $isPk = false)
{
    $hint = '\DateTime' === convertPHPType($options['type']) ? getDateTimeObject($options['type']) . ' ' : null;

    $null = null;

    if (false === $isPk) {
        $null = true === $options['nullable'] ? ' = null' : null;
    }

    $lines = [];

    if (null !== $hint) {
        $type = trim($hint);

    } else {
        $type = convertPHPType($options['type']);
    }

    $lines[] = '<space>/**';
    $lines[] = '<space> * Set ' . $property;
    $lines[] = '<space> *';
    $lines[] = '<space> * @param '. sprintf('%s $%s', $type, $property);
    $lines[] = '<space> * @return ' . $entityName;
    $lines[] = '<space> */';
    $lines[] = '<space>public function set' . sprintf('%s(%s$%s%s)', ucfirst($property), $hint, $property, $null);
    $lines[] = '<space>{';
    $lines[] = '<space><space>$this->' . sprintf('%s = $%s;', $property, $property);
    $lines[] = '<space><space>return $this;';
    $lines[] = '<space>}';
    $lines[] = '';

    $lines[] = '<space>/**';
    $lines[] = '<space> * Get ' . $property;
    $lines[] = '<space> *';
    $lines[] = '<space> * @return ' . $type;
    $lines[] = '<space> */';
    $lines[] = '<space>public function get' . sprintf('%s()', ucfirst($property));
    $lines[] = '<space>{';
    $lines[] = '<space><space>return $this->' . sprintf('%s;', $property);
    $lines[] = '<space>}';
    $lines[] = '';

    return $lines;
}

function getRepository($key) {
    $repos = [];
    $repos['administracao.atributo_dinamico'] = 'Urbem\CoreBundle\Repository\Administracao\AtributoDinamicoRepository';
    $repos['administracao.configuracao'] = 'Urbem\CoreBundle\Repository\Administracao\ConfiguracaoRepository';
    $repos['administracao.funcao'] = 'Urbem\CoreBundle\Repository\Administracao\FuncaoRepository';
    $repos['administracao.modulo'] = 'Urbem\CoreBundle\Repository\Administracao\ModuloRepository';
    $repos['administracao.rota'] = 'Urbem\CoreBundle\Repository\Administracao\RotaRepository';
    $repos['almoxarifado.almoxarifado'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\AlmoxarifadoRepository';
    $repos['almoxarifado.catalogo_classificacao'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\CatalogoClassificacaoRepository';
    $repos['almoxarifado.catalogo_item'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\CatalogoItemRepository';
    $repos['almoxarifado.centro_custo'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\CentroCustoRepository';
    $repos['almoxarifado.centro_custo_permissao'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\CentroCustoPermissaoRepository';
    $repos['almoxarifado.inventario'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\InventarioRepository';
    $repos['almoxarifado.inventario_itens'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\InventarioItensRepository';
    $repos['almoxarifado.pedido_transferencia_item'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\PedidoTransferenciaItemRepository';
    $repos['almoxarifado.requisicao'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\RequisicaoRepository';
    $repos['almoxarifado.requisicao_item'] = 'Urbem\CoreBundle\Repository\Patrimonio\Almoxarifado\RequisicaoItemRepository';
    $repos['beneficio.vigencia'] = 'Urbem\CoreBundle\Repository\Beneficio\VigenciaRepository';
    $repos['calendario.feriado'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Calendario\FeriadoRepository';
    $repos['compras.compra_direta'] = 'Urbem\CoreBundle\Repository\Patrimonio\Compras\CompraDiretaRepository';
    $repos['compras.fornecedor'] = 'Urbem\CoreBundle\Repository\Patrimonio\Compras\FornecedorRepository';
    $repos['compras.julgamento_item'] = 'Urbem\CoreBundle\Repository\Patrimonio\Compras\JulgamentoItemRepository';
    $repos['compras.mapa'] = 'Urbem\CoreBundle\Repository\Patrimonio\Compras\MapaRepository';
    $repos['compras.mapa_item'] = 'Urbem\CoreBundle\Repository\Patrimonio\Compras\MapaItemRepository';
    $repos['compras.ordem'] = 'Urbem\CoreBundle\Repository\Patrimonio\Compras\OrdemRepository';
    $repos['compras.solicitacao'] = 'Urbem\CoreBundle\Repository\Patrimonio\Compras\SolicitacaoRepository';
    $repos['compras.solicitacao_anulacao'] = 'Urbem\CoreBundle\Repository\Patrimonio\Compras\SolicitacaoAnulacaoRepository';
    $repos['concurso.edital'] = 'Urbem\CoreBundle\Repository\Concurso\EditalRepository';
    $repos['contabilidade.desdobramento_receita'] = 'Urbem\CoreBundle\Repository\Contabilidade\DesdobramentoReceitaRepository';
    $repos['contabilidade.encerramento_mes'] = 'Urbem\CoreBundle\Repository\Contabilidade\EncerramentoMesRepository';
    $repos['contabilidade.lancamento'] = 'Urbem\CoreBundle\Repository\Contabilidade\LancamentoRepository';
    $repos['contabilidade.lancamento_retencao'] = 'Urbem\CoreBundle\Repository\Contabilidade\LancamentoRetencaoRepository';
    $repos['contabilidade.plano_conta'] = 'Urbem\CoreBundle\Repository\Contabilidade\PlanoContaRepository';
    $repos['cse.cidadao'] = 'Urbem\CoreBundle\Repository\Cse\CidadaoRepository';
    $repos['economico.responsavel_tecnico'] = 'Urbem\CoreBundle\Repository\Economico\ResponsavelTecnicoRepository';
    $repos['estagio.curso_instituicao_ensino'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Estagio\CursoInstituicaoEnsinoRepository';
    $repos['folhapagamento.concessao_decimo'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\ConcessaoDecimoRepository';
    $repos['folhapagamento.configuracao_empenho_evento'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\ConfiguracaoEmpenhoEventoRepository';
    $repos['folhapagamento.configuracao_empenho_lla_atributo_valor'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\ConfiguracaoEmpenhoLlaAtributoValorRepository';
    $repos['folhapagamento.configuracao_empenho_lla_local'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\ConfiguracaoEmpenhoLlaLocalRepository';
    $repos['folhapagamento.configuracao_empenho_lla_lotacao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\ConfiguracaoEmpenhoLlaLotacaoRepository';
    $repos['folhapagamento.desconto_externo_previdencia'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\FolhaPagamento\DescontoExternoPrevidenciaRepository';
    $repos['folhapagamento.evento'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\EventoRepository';
    $repos['folhapagamento.folha_situacao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\FolhaSituacaoRepository';
    $repos['folhapagamento.padrao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\PadraoRepository';
    $repos['folhapagamento.pensao_evento'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\PensaoEventoRepository';
    $repos['folhapagamento.pensao_funcao_padrao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\PensaoFuncaoPadraoRepository';
    $repos['folhapagamento.periodo_movimentacao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\PeriodoMovimentacaoRepository';
    $repos['folhapagamento.registro_evento_periodo'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\RegistroEventoPeriodoRepository';
    $repos['folhapagamento.sequencia_calculo'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\SequenciaCalculoRepository';
    $repos['folhapagamento.totais_folha_eventos'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\TotaisFolhaEventosRepository';
    $repos['folhapagamento.ultimo_registro_evento_decimo'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Folhapagamento\UltimoRegistroEventoDecimoRepository';
    $repos['frota.item'] = 'Urbem\CoreBundle\Repository\Patrimonio\Frota\ItemRepository';
    $repos['frota.marca'] = 'Urbem\CoreBundle\Repository\Patrimonio\MarcaRepository';
    $repos['frota.motorista'] = 'Urbem\CoreBundle\Repository\Patrimonio\Frota\MotoristaRepository';
    $repos['frota.posto'] = 'Urbem\CoreBundle\Repository\Patrimonio\PostoRepository';
    $repos['frota.proprio'] = 'Urbem\CoreBundle\Repository\Patrimonio\Frota\ProprioRepository';
    $repos['frota.veiculo'] = 'Urbem\CoreBundle\Repository\Patrimonio\Frota\VeiculoRepository';
    $repos['ldo.ldo'] = 'Urbem\CoreBundle\Repository\Ldo\LdoRepository';
    $repos['licitacao.comissao'] = 'Urbem\CoreBundle\Repository\Patrimonio\Licitacao\ComissaoRepository';
    $repos['licitacao.contrato_aditivos'] = 'Urbem\CoreBundle\Repository\Patrimonio\ContratoAditivoRepository';
    $repos['licitacao.edital'] = 'Urbem\CoreBundle\Repository\Patrimonio\Licitacao\EditalRepository';
    $repos['licitacao.licitacao'] = 'Urbem\CoreBundle\Repository\Patrimonio\Licitacao\LicitacaoRepository';
    $repos['normas.norma'] = 'Urbem\CoreBundle\Repository\Normas\NormaRepository';
    $repos['orcamento.conta_despesa'] = 'Urbem\CoreBundle\Repository\Orcamento\ContaDespesaRepository';
    $repos['orcamento.conta_receita'] = 'Urbem\CoreBundle\Repository\Orcamento\ContaReceitaRepository';
    $repos['orcamento.despesa'] = 'Urbem\CoreBundle\Repository\Orcamento\DespesaRepository';
    $repos['orcamento.entidade'] = 'Urbem\CoreBundle\Repository\Orcamento\EntidadeRepository';
    $repos['orcamento.pao'] = 'Urbem\CoreBundle\Repository\Orcamento\PaoRepository';
    $repos['orcamento.receita'] = 'Urbem\CoreBundle\Repository\Orcamento\ReceitaRepository';
    $repos['orcamento.suplementacao'] = 'Urbem\CoreBundle\Repository\Orcamento\SuplementacaoRepository';
    $repos['organograma.organograma'] = 'Urbem\CoreBundle\Repository\Organograma\OrganogramaRepository';
    $repos['organograma.orgao'] = 'Urbem\CoreBundle\Repository\Organograma\OrgaoRepository';
    $repos['patrimonio.apolice'] = 'Urbem\CoreBundle\Repository\Patrimonio\Patrimonio\ApoliceRepository';
    $repos['patrimonio.bem'] = 'Urbem\CoreBundle\Repository\Patrimonio\BemRepository';
    $repos['patrimonio.especie_atributo'] = 'Urbem\CoreBundle\Repository\Patrimonio\EspecieAtributoRepository';
    $repos['patrimonio.grupo'] = 'Urbem\CoreBundle\Repository\Patrimonio\GrupoRepository';
    $repos['patrimonio.inventario'] = 'Urbem\CoreBundle\Repository\Patrimonio\Patrimonio\InventarioRepository';
    $repos['patrimonio.natureza'] = 'Urbem\CoreBundle\Repository\Patrimonio\NaturezaRepository';
    $repos['pessoal.atributo_contrato_servidor_valor'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\AtributoContratoServidorValorRepository';
    $repos['pessoal.cargo'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\CargoRepository';
    $repos['pessoal.cargo_requisito'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\CargoRequisitoRepository';
    $repos['pessoal.caso_causa'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\CasoCausaRepository';
    $repos['pessoal.categoria'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\CategoriaRepository';
    $repos['pessoal.contrato'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoRepository';
    $repos['pessoal.contrato_servidor'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorRepository';
    $repos['pessoal.contrato_servidor_caso_causa'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorCasoCausaRepository';
    $repos['pessoal.contrato_servidor_conta_salario'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorContaSalarioRepository';
    $repos['pessoal.contrato_servidor_forma_pagamento'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorFormaPagamentoRepository';
    $repos['pessoal.contrato_servidor_funcao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorFuncaoRepository';
    $repos['pessoal.contrato_servidor_local'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorLocalRepository';
    $repos['pessoal.contrato_servidor_ocorrencia'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorOcorrenciaRepository';
    $repos['pessoal.contrato_servidor_orgao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorOrgaoRepository';
    $repos['pessoal.contrato_servidor_padrao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorPadraoRepository';
    $repos['pessoal.contrato_servidor_previdencia'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorPrevidenciaRepository';
    $repos['pessoal.contrato_servidor_regime_funcao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorRegimeFuncaoRepository';
    $repos['pessoal.contrato_servidor_sindicato'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorSindicatoRepository';
    $repos['pessoal.contrato_servidor_sub_divisao_funcao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ContratoServidorSubDivisaoFuncaoRepository';
    $repos['pessoal.dias_turno'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\DiasTurnoRepository';
    $repos['pessoal.enquadramento'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\EnquadramentoRepository';
    $repos['pessoal.faixa_turno'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\FaixaTurnoRepository';
    $repos['pessoal.grade_horario'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\GradeHorarioRepository';
    $repos['pessoal.mov_sefip_retorno'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\MovSefipRetornoRepository';
    $repos['pessoal.mov_sefip_saida'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\MovSefipSaidaRepository';
    $repos['pessoal.mov_sefip_saida_categoria'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\MovSefipSaidaCategoriaRepository';
    $repos['pessoal.mov_sefip_saida_mov_sefip_retorno'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\MovSefipSaidaMovSefipRetornoRepository';
    $repos['pessoal.pensao'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\PensaoRepository';
    $repos['pessoal.regime'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\RegimeRepository';
    $repos['pessoal.sefip'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\SefipRepository';
    $repos['pessoal.servidor'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ServidorRepository';
    $repos['pessoal.servidor_dependente'] = 'Urbem\CoreBundle\Repository\RecursosHumanos\Pessoal\ServidorDependenteRepository';
    $repos['ppa.acao'] = 'Urbem\CoreBundle\Repository\Ppa\AcaoRepository';
    $repos['ppa.ppa'] = 'Urbem\CoreBundle\Repository\Ppa\PpaRepository';
    $repos['protocolo.assunto_acao'] = 'Urbem\CoreBundle\Repository\Protocolo\AssuntoAcaoRepository';
    $repos['sw_assunto_atributo'] = 'Urbem\CoreBundle\Repository\SwAssuntoAtributoRepository';
    $repos['sw_cgm'] = 'Urbem\CoreBundle\Repository\SwCgmRepository';
    $repos['sw_cgm_pessoa_fisica'] = 'Urbem\CoreBundle\Repository\SwCgmPessoaFisicaRepository';
    $repos['sw_cgm_pessoa_juridica'] = 'Urbem\CoreBundle\Repository\SwCgmPessoaJuridicaRepository';
    $repos['sw_documento_processo'] = 'Urbem\CoreBundle\Repository\SwDocumentoProcessoRepository';
    $repos['sw_processo'] = 'Urbem\CoreBundle\Repository\SwProcessoRepository';
    $repos['tesouraria.saldo_tesouraria'] = 'Urbem\CoreBundle\Repository\Financeiro\Tesouraria\SaldoTesourariaRepository';

    return true === array_key_exists($key, $repos) ? $repos[$key] : null;
}

function getTables(PDO $dbh, $entityNamespace, array $remove) {
    $sql = [];

    foreach ($remove as $schema => $tables) {
        foreach ($tables as $table) {
            $sql[] = sprintf("(table_schema || '.' || table_name != '%s.%s')", $schema, $table);
        }
    }

    $join = 0 < count($sql) ? 'AND ' : '';

    $stmt = $dbh->prepare($sql = '
        SELECT  quote_ident(table_name) AS table,
                table_schema AS schema
         FROM   information_schema.tables
        WHERE   table_schema NOT LIKE \'pg_%\'
          AND   table_schema != \'information_schema\'
         ' . $join . implode(" AND ", $sql));

    $stmt->execute();

    $tables = [];

    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $table) {
        $key = $table['schema'] . '.' . $table['table'];
        $key = str_replace('public.', '', $key);

        $tables[$key] = ['table' => $table['table'], 'schema' => str_replace('public', '', $table['schema'])];
        $tables[$key]['columns'] = [];
        $tables[$key]['indexes'] = [];
        $tables[$key]['foreignKeys'] = [
            'manyToOne' => [],
            'oneToMany' => [],
            'oneToOneOwningSide' => [],
            'oneToOneInverseSide' => [],
            'relation' => [],
        ];
    }

    return getColumns($dbh, $tables, $entityNamespace);
}

function getColumns(PDO $dbh, $tables, $entityNamespace) {
    $doctrineTypeMapping = array(
        'hstore'        => 'hstore',
        'hstore'        => 'blob',
        'USER-DEFINED' => 'blob',
        'character varying' => 'string',
        'character'     => 'string',
        'smallint'      => 'smallint',
        'int2'          => 'smallint',
        'serial'        => 'integer',
        'oid'           => 'blob',
        'serial4'       => 'integer',
        'int'           => 'integer',
        'int4'          => 'integer',
        'integer'       => 'integer',
        'bigserial'     => 'bigint',
        'serial8'       => 'bigint',
        'bigint'        => 'bigint',
        'int8'          => 'bigint',
        'bool'          => 'boolean',
        'boolean'       => 'boolean',
        'text'          => 'text',
        'varchar'       => 'string',
        'interval'      => 'string',
        '_varchar'      => 'string',
        'char'          => 'string',
        'bpchar'        => 'string',
        'inet'          => 'string',
        'date'          => 'date',
        'timestamp with time zone' => 'datetime',
        'timestamp without time zone' => 'datetime',
        'datetime'      => 'datetime',
        'timestamp'     => 'datetime',
        'timestamptz'   => 'datetime',
        'time without time zone' => 'time',
        'time'          => 'time',
        'timetz'        => 'time',
        'float'         => 'float',
        'float4'        => 'float',
        'float8'        => 'float',
        'double'        => 'float',
        'double precision' => 'float',
        'real'          => 'float',
        'decimal'       => 'decimal',
        'money'         => 'decimal',
        'numeric'       => 'decimal',
        'year'          => 'date',
        'uuid'          => 'guid',
        'bytea'         => 'blob',
    );

    $stmt = $dbh->prepare('
         SELECT c.table_schema AS schema, c.table_name AS table, c.column_name AS column, c.column_default AS default,
             c.is_nullable AS nullable, c.data_type AS type, c.character_maximum_length AS maximum_length, c.numeric_precision, c.numeric_scale
        FROM information_schema.columns c 
       WHERE c.table_schema NOT LIKE \'pg%\'
         AND c.table_schema != \'information_schema\'
         ORDER BY c.table_schema, c.table_name, c.ordinal_position
     ');

    $stmt->execute();

    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $column) {
        $key = $column['schema'] . '.' . $column['table'];
        $key = str_replace('public.', '', $key);

        if (false === array_key_exists($key, $tables)) {
            continue;
        }

        $type = $doctrineTypeMapping[$column['type']];
        $length = $precision = $scale = $columnDefinition = null;

        if ('string' == $type) {
            $length = $column['maximum_length'];
        }

        $default = $column['default'];

        $sequence = null;

        if (0 === strpos($default, 'nextval')) {
            $sequence = str_replace(['nextval(\'', '\'::regclass)'], '', $default);
            $default = null;

        } else {
            $default = explode('::', $default);
            $default = reset($default);
            $default = str_replace(['"', "'", "(", ")"], '', trim($default));
            $default = $default === 'true' ? 1 : $default;
            $default = $default === 'false' ? 0 : $default;
            $default = strpos($default, 'next') === 0 ? null : $default;
            $default = $default === '0.00' ? 0 : $default;
            $default = strtolower($default) === 'null' ? null : $default;
            $default = strlen($default) === 0 ? null : $default;

            if ('decimal' == $type) {
                $precision = $column['numeric_precision'];
                $scale = $column['numeric_scale'];

                $default = trim($default, "'");
                $default = $default === '' ? null : (int) $default;
            }
        }

        $tables[$key]['columns'][] = [
            'name' => str_replace('"', '', $column['column']),
            'type' => $type,
            'nullable' => 'YES' == $column['nullable'],
            'pk' => false,
            'length' => $length,
            'precision' => $precision,
            'scale' => $scale,
            'default' => $default,
            'sequence' => $sequence,
        ];
    }

    return getIndexes($dbh, $tables, $entityNamespace);
}

function getIndexes(PDO $dbh, $tables, $entityNamespace)
{
    $stmt = $dbh->prepare('
        SELECT  idx.indrelid :: REGCLASS AS key,
                i.relname AS name,
                idx.indisunique AS unique,
                idx.indisprimary AS pk,
                array_to_json(array(SELECT  pg_get_indexdef(idx.indexrelid, k + 1, TRUE)
                        FROM  generate_subscripts(idx.indkey, 1) AS k
                    ORDER BY k
                )) AS index
          FROM  pg_index AS idx
          JOIN  pg_class AS i ON i.oid = idx.indexrelid
          JOIN  pg_am AS am ON i.relam = am.oid
          JOIN  pg_namespace AS NS ON i.relnamespace = NS.OID
          JOIN  pg_user AS U ON i.relowner = U.usesysid
     WHERE NOT nspname LIKE \'pg%\' AND NOT nspname LIKE \'information_schema\'
     ');

    $stmt->execute();

    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $index) {
        $key = str_replace('public.', '', $index['key']);

        if (false === array_key_exists($key, $tables)) {
            continue;
        }

        $columns = json_decode($index['index']);

        array_walk($columns, function (&$column) {
            $column = str_replace('"', '', $column);
        });

        $tables[$key]['indexes'][] = [
            'unique' => $index['unique'],
            'name' => $index['name'],
            'columns' => $columns,
        ];

        if (true === $index['pk']) {
            foreach ($columns as $column) {
                foreach ($tables[$key]['columns'] as &$tableColumns) {
                    if ($tableColumns['name'] === $column) {
                        $tableColumns['pk'] = true;
                    }
                }
            }
        }
    }

    return getForeignKeys($dbh, $tables, $entityNamespace);
}

function getForeignKeys(PDO $dbh, $tables, $entityNamespace)
{
    $stmt = $dbh->prepare('
        SELECT  conname AS name, source_table::regclass AS source, source_attr.attname AS source_column, target_table::regclass AS target, target_attr.attname AS target_column
          FROM  pg_attribute target_attr, pg_attribute source_attr,
                (SELECT   conname, source_table, target_table, source_constraints[i] source_constraints, target_constraints[i] AS target_constraints
                   FROM   (SELECT conname, conrelid as source_table, confrelid AS target_table, conkey AS source_constraints, confkey AS target_constraints, generate_series(1, array_upper(conkey, 1)
                ) AS i 
                  FROM pg_constraint WHERE contype = \'f\'
                ) query1
                ) query2
        WHERE   target_attr.attnum = target_constraints AND target_attr.attrelid = target_table AND source_attr.attnum = source_constraints AND source_attr.attrelid = source_table;
     ');

    $stmt->execute();

    $getEntityNamespace = function($key) use ($entityNamespace) {
        if (count($key) == 2) {
            $key = sprintf($entityNamespace . '%s\%s', camelize($key[0]), camelize($key[1]));

        } else {
            $key = sprintf($entityNamespace . '%s', camelize($key[0]));
        }

        return $key;
    };

    $rules = array(
        'ao'    => 'oes',
        'es'    => 'eses',
        'm'     => 'ns',
        'l'     => 'is',
        'r'     => 'res',
        'x'     => 'xes',
        'z'     => 'zes',
    );

    $plural = function($name) use ($rules) {
        foreach($rules as $singular => $plural) {
            if (preg_match("({$singular}$)", $name)) {
                return preg_replace("({$singular}$)", $plural, $name);
            }
        }

        return true === (substr($name, -1) !== 's') ? $name . 's' : $name;
    };

    $getProperty = function($name) {
        $name = explode('.', $name);
        $name = count($name) == 1 ? camelize(reset($name)) : camelize(reset($name)) . ucfirst(camelize(end($name)));

        return lcfirst('fk' . $name);
    };

    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $foreignKey) {
        $key = str_replace('public.', '', $foreignKey['source']);
        $name = $foreignKey['name'];

        if (false === array_key_exists($key, $tables)) {
            continue;
        }

        $sourceColumns = $targetColumns = [];

        $target = str_replace('public.', '', $foreignKey['target']);
        $source =  str_replace('public.', '', $foreignKey['source']);

        $property = $getProperty($target);

        if (true == array_key_exists($name, $tables[$key]['foreignKeys']['relation'])) {
            $sourceColumns = $tables[$key]['foreignKeys']['relation'][$name]['source_columns'];
            $targetColumns = $tables[$key]['foreignKeys']['relation'][$name]['target_columns'];
        }

        $sourceColumns[] = $foreignKey['source_column'];
        $targetColumns[] = $foreignKey['target_column'];

        $inversedBy = $plural($getProperty($source));

        $target = $getEntityNamespace(explode('.', $target));
        $source = $getEntityNamespace(explode('.', $source));

        $relation = [
            'property' => $property,
            'source' => $source,
            'target' => $target,
            'source_columns' => $sourceColumns,
            'target_columns' => $targetColumns,
            'inversedBy' => $inversedBy,
            'inversedByKey' => str_replace('public.', '', $foreignKey['target']),
        ];

        $tables[$key]['foreignKeys']['relation'][$name] = $relation;
    }

    $tables = createOneToOneAndManyToOne($tables, $entityNamespace);

    return $tables;
}

function camelize($input, $separator = '_') {
    return str_replace($separator, '', ucwords($input, $separator));
}

function resetTree($dir, $begin = true) {
    if (true === $begin && false === is_dir($dir)) {
        mkdir($dir, 0777, true);
    }

    $files = array_diff(scandir($dir), array('.','..'));

    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? resetTree("$dir/$file", false) : unlink("$dir/$file");
    }

    rmdir($dir);

    if (true === $begin) {
        mkdir($dir, 0777, true);
    }
}

function toYAML(PDO $pdo, $table, $entityNamespace) {
    $entity = [];
    $entity['type'] = 'entity';
    $entity['table'] = trim($table['schema'] . '.' . $table['table'], '.');

    if (null !== $repo = getRepository($entity['table'])) {
        $entity['repositoryClass'] = $repo;
    }

    $ids = array_filter($table['columns'], function ($column) {
        return true === $column['pk'];
    });

    $columns = array_filter($table['columns'], function ($column) {
        return false === $column['pk'];
    });

    $uniqueIndexes = array_filter($table['indexes'], function ($index) {
        return true === $index['unique'];
    });

    if(0 < count($uniqueIndexes) && 0 < count($ids)) {
        $idColumns = [];

        array_walk($ids, function($id) use (&$idColumns) {
            $idColumns[] = $id['name'];
        });

        $uniqueIndexes = array_filter($uniqueIndexes, function($uniqueIndex) use ($idColumns) {
            return count(array_intersect($uniqueIndex['columns'], $idColumns)) !== count($idColumns);
        });
    }

    $indexes = array_filter($table['indexes'], function ($index) {
        return false === $index['unique'];
    });

    // view?
    if (0 === count($ids)) {
        $ids[] = array_shift($columns);
    }

    // manyToMany?
    if (0 === count($columns)) {

    }

    // indexes
    if (0 < count($indexes)) {
        $entity['indexes'] = [];

        foreach ($indexes as $index) {
            $entity['indexes'][$index['name']] = ['columns' => []];

            foreach ($index['columns'] as $column) {
                $entity['indexes'][$index['name']]['columns'][] = $column;
            }
        }
    }

    // uniqueConstraints
    if (0 < count($uniqueIndexes)) {
        $entity['uniqueConstraints'] = [];

        foreach ($uniqueIndexes as $uniqueIndex) {
            $entity['uniqueConstraints'][$uniqueIndex['name']] = ['columns' => []];

            foreach ($uniqueIndex['columns'] as $column) {
                $entity['uniqueConstraints'][$uniqueIndex['name']]['columns'][] = $column;
            }
        }
    }

    $entity['id'] = [];

    $lifecycleCallbacks = [];

    // id
    foreach ($ids as $id) {
        $name = $id['name'];
        $property = lcfirst(camelize($name));

        $entity['id'][$property] = [];
        $entity['id'][$property]['type'] = convertDoctrineType($pdo, $id['type'], $name, $entity['table'], true);
        $entity['id'][$property]['generator'] = [];
        $entity['id'][$property]['generator']['strategy'] = 'NONE';

        if (null !== $id['sequence'] && 1 < count($ids)) {
            $lifecycleCallbacks['prePersist'] = ['generatePkSequence'];

        } elseif (null !== $id['sequence'] && 1 == count($ids)) {
            $entity['id'][$property]['generator']['strategy'] = 'SEQUENCE';
            $entity['id'][$property]['sequenceGenerator'] = [];
            $entity['id'][$property]['sequenceGenerator']['sequenceName'] = $id['sequence'];
            $entity['id'][$property]['sequenceGenerator']['allocationSize'] = 1;
            $entity['id'][$property]['sequenceGenerator']['initialValue'] = 1;
        }

        if ($property !== $name) {
            $entity['id'][$property]['column'] = $name;
        }
    }

    $entity['fields'] = [];

    // fields
    foreach ($columns as $column) {
        $name = $column['name'];
        $property = lcfirst(camelize($name));

        $entity['fields'][$property] = [];
        $entity['fields'][$property]['type'] = convertDoctrineType($pdo, $column['type'], $name, $entity['table']);
        $entity['fields'][$property]['nullable'] = $column['nullable'];

        if ($column['precision'] !== null) {
            $entity['fields'][$property]['precision'] = $column['precision'];
        }

        if ($column['scale'] !== null) {
            $entity['fields'][$property]['scale'] = $column['scale'];
        }

        if ($column['length'] !== null) {
            $entity['fields'][$property]['length'] = $column['length'];
        }

        if ($column['default'] !== null && false === in_array($column['type'], ['datetime', 'time', 'date'])) {
            $entity['fields'][$property]['options'] = ['default' => $column['default']];
        }

        if ($property !== $name) {
            $entity['fields'][$property]['column'] = $name;
        }
    }

    // manyToOne
    if (0 < count($table['foreignKeys']['manyToOne'])) {
        $entity['manyToOne'] = [];

        foreach ($table['foreignKeys']['manyToOne'] as $manyToOne) {
            $property = $manyToOne['property'];

            $entity['manyToOne'][$property] = [];
            $entity['manyToOne'][$property]['targetEntity'] = $manyToOne['target'];
            $entity['manyToOne'][$property]['inversedBy'] = $manyToOne['inversedBy'];

            $entity['manyToOne'][$property]['joinColumns'] = [];

            foreach ($manyToOne['source_columns'] as $index => $column) {
                $entity['manyToOne'][$property]['joinColumns'][$column] = [
                    'referencedColumnName' => $manyToOne['target_columns'][$index]
                ];
            };
        }
    }

    // oneToMany
    if (0 < count($table['foreignKeys']['oneToMany'])) {
        $entity['oneToMany'] = [];

        foreach ($table['foreignKeys']['oneToMany'] as $property => $oneToMany) {
            $property = $oneToMany['property'];

            $entity['oneToMany'][$property] = [];
            $entity['oneToMany'][$property]['cascade'] = ['persist', 'remove'];
            $entity['oneToMany'][$property]['orphanRemoval'] = true;
            $entity['oneToMany'][$property]['targetEntity'] = $oneToMany['target'];
            $entity['oneToMany'][$property]['mappedBy'] = $oneToMany['mappedBy'];
        }
    }

    if (0 < count($table['foreignKeys']['oneToOneOwningSide']) || 0 < count($table['foreignKeys']['oneToOneInverseSide'])) {
        $entity['oneToOne'] = [];
    }

    $oneToOneOwningSideList = $oneToOneInverseSideList = [];

    // oneToOneOwningSide
    if (0 < count($table['foreignKeys']['oneToOneOwningSide'])) {
        foreach ($table['foreignKeys']['oneToOneOwningSide'] as $oneToOneOwningSide) {
            $property = $oneToOneOwningSide['property'];

            $entity['oneToOne'][$property] = [];
            $entity['oneToOne'][$property]['cascade'] = ['persist', 'remove'];
            $entity['oneToOne'][$property]['targetEntity'] = $oneToOneOwningSide['target'];
            $entity['oneToOne'][$property]['mappedBy'] = $oneToOneOwningSide['inversedBy'];

            $entity['oneToOne'][$property]['joinColumns'] = [];

            foreach ($oneToOneOwningSide['target_columns'] as $index => $column) {
                $entity['oneToOne'][$property]['joinColumns'][$column] = [
                    'referencedColumnName' => $oneToOneOwningSide['source_columns'][$index]
                ];
            };

            $oneToOneOwningSideList[$property] = $entity['oneToOne'][$property];
        }
    }

    // oneToOneInverseSide
    if (0 < count($table['foreignKeys']['oneToOneInverseSide'])) {
        foreach ($table['foreignKeys']['oneToOneInverseSide'] as $property => $oneToOneInverseSide) {
            $property = $oneToOneInverseSide['property'];

            $entity['oneToOne'][$property] = [];
            $entity['oneToOne'][$property]['targetEntity'] = $oneToOneInverseSide['target'];
            $entity['oneToOne'][$property]['inversedBy'] = $oneToOneInverseSide['mappedBy'];

            $entity['oneToOne'][$property]['joinColumns'] = [];

            foreach ($oneToOneInverseSide['source_columns'] as $index => $column) {
                $entity['oneToOne'][$property]['joinColumns'][$column] = [
                    'referencedColumnName' => $oneToOneInverseSide['target_columns'][$index]
                ];
            };

            $oneToOneInverseSideList[$property] = $entity['oneToOne'][$property];
        }
    }

    if ('' !== $table['schema']) {
        $name = sprintf($entityNamespace . '%s\%s', camelize($table['schema']), camelize($table['table']));

    } else {
        $name = sprintf($entityNamespace . '%s', camelize($table['table']));
    }

    if (0 < count($lifecycleCallbacks)) {
        $entity['lifecycleCallbacks'] = $lifecycleCallbacks;
    }

    return [
        [
            $name => $entity + [
                'oneToOneOwningSide' => $oneToOneOwningSideList,
                'oneToOneInverseSide' => $oneToOneInverseSideList,
            ],
        ],
        \Symfony\Component\Yaml\Yaml::dump([$name => $entity], 10)
    ];
}

function toEntity($entity, $table, $entityNamespace)
{
    $classTemplate =
'<?php
 
<namespace>
<entityAnnotation>
<entityClassName> 
{
<entityBody>
<constructor>
<settersAndGetters>
<lifecycle>
}';

    $getEntityDocBlock = function($entityName) {
        $lines = [];
        $lines[] = '/**';
        $lines[] = ' * ' . $entityName;
        $lines[] = ' */';

        return implode("\n", $lines);
    };

    $getColumn = function($property, $options) use ($table) {
        $column = array_filter($table['columns'], function($column) use ($property, $options) {
            $name = true === array_key_exists('column', $options) ? $options['column'] : $property;
            return $column['name'] === $name;
        });

        return reset($column);
    };

    $getDefaultColumn = function($property, $options) use ($getColumn) {
        return $getColumn($property, $options)['default'];
    };

    $getDefault = function($property, $options, $isDeclaration = false) use($table, $getDefaultColumn) {
        $default = $getDefaultColumn($property, $options);

        if (true === $isDeclaration && 'now' === strtolower($default)) {
            $default = null;
        }

        if (null !== $default) {
            if ('boolean' === $options['type']) {
                $default = 0 === (int)$default ? 'false' : 'true';

            } elseif (true === in_array($options['type'], ['decimal', 'integer'])) {
                $default = (int) $default;

            } else {
                $default = is_string($default) ? sprintf("'%s'", $default) : $default;
            }

            $default = sprintf(' = %s', $default);
        }

        return $default;
    };

    $getEntityProperties = function($entity) use ($table, $getDefault, $getColumn) {
        $lines = [];

        foreach ($entity['id'] as $property => $options) {
            $type = convertPHPType($options['type']);
            $type = '\DateTime' === $type ? getDateTimeObject($options['type']) : $type;

            $lines[] = '<space>/**';
            $lines[] = '<space> * PK';
            $lines[] = '<space> * @var ' . $type;
            $lines[] = '<space> */';
            $lines[] = sprintf('<space>private $%s%s;', $property, $getDefault($property, $options, true));
            $lines[] = '';
        }

        foreach ($entity['fields'] as $property => $options) {
            $lines[] = '<space>/**';
            $lines[] = '<space> * @var ' . convertPHPType($options['type']);
            $lines[] = '<space> */';
            $lines[] = sprintf('<space>private $%s%s;', $property, $getDefault($property, $options, true));
            $lines[] = '';
        }

        if (true === array_key_exists('oneToOneOwningSide', $entity)) {
            foreach ($entity['oneToOneOwningSide'] as $property => $options) {
                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToOne (inverse side)';
                $lines[] = '<space> * @var \\' . $options['targetEntity'];
                $lines[] = '<space> */';
                $lines[] = sprintf('<space>private $%s;', $property);
                $lines[] = '';
            }
        }

        if (true === array_key_exists('oneToOneInverseSide', $entity)) {
            foreach ($entity['oneToOneInverseSide'] as $property => $options) {
                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToOne (owning side)';
                $lines[] = '<space> * @var \\' . $options['targetEntity'];
                $lines[] = '<space> */';
                $lines[] = sprintf('<space>private $%s;', $property);
                $lines[] = '';
            }
        }

        if (true === array_key_exists('oneToMany', $entity)) {
            foreach ($entity['oneToMany'] as $property => $options) {
                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToMany';
                $lines[] = sprintf('<space> * @var \Doctrine\Common\Collections\Collection|\%s', $options['targetEntity']);
                $lines[] = '<space> */';
                $lines[] = sprintf('<space>private $%s;', $property);
                $lines[] = '';
            }
        }

        if (true === array_key_exists('manyToOne', $entity)) {
            foreach ($entity['manyToOne'] as $property => $options) {
                $lines[] = '<space>/**';
                $lines[] = '<space> * ManyToOne';
                $lines[] = '<space> * @var \\' . $options['targetEntity'];
                $lines[] = '<space> */';
                $lines[] = sprintf('<space>private $%s;', $property);
                $lines[] = '';
            }
        }

        return implode("\n", $lines);
    };

    $getEntityConstructor = function($entity) use ($getDefaultColumn) {
        $constructor = false;
        $lines = [];

        $constructorLines[] = '<space>/**';
        $constructorLines[] = '<space> * Constructor';
        $constructorLines[] = '<space> */';
        $constructorLines[] = '<space>public function __construct()';
        $constructorLines[] = '<space>{';

        if (true === array_key_exists('oneToMany', $entity)) {
            $constructor = true;

            $lines+= $constructorLines;

            foreach ($entity['oneToMany'] as $property => $options) {
                $lines[] = sprintf('<space><space>$this->%s = new \Doctrine\Common\Collections\ArrayCollection();', $property);
            }
        }

        foreach ($entity['id'] as $property => $options) {
            if ('\DateTime' === convertPHPType($options['type'])) {
                if (false === $constructor) {
                    $constructor = true;
                    $lines+= $constructorLines;
                }

                $lines[] = sprintf('<space><space>$this->%s = new %s;', $property, getDateTimeObject($options['type']));
            }
        }

        foreach ($entity['fields'] as $property => $options) {
            $default = $getDefaultColumn($property, $options);

            if (null !== $default && '\DateTime' === convertPHPType($options['type'])) {
                if (false === $constructor) {
                    $constructor = true;
                    $lines+= $constructorLines;
                }

                $lines[] = sprintf('<space><space>$this->%s = new %s;', $property, getDateTimeObject($options['type']));
            }
        }

        if (true === $constructor) {
            $lines[] = '<space>}';
            $lines[] = '';
        }

        return implode("\n", $lines);
    };

    $getEntitySettersAndGetters = function($entity, $entityName, $entityNamespace) {
        $lines = [];

        foreach ($entity['id'] as $property => $options) {
            $lines = array_merge($lines, buildSetterAndGetter($entityName, $property, $options, true));
        }

        foreach ($entity['fields'] as $property => $options) {
            $lines = array_merge($lines, buildSetterAndGetter($entityName, $property, $options));
        }

        if (true === array_key_exists('oneToMany', $entity)) {
            foreach ($entity['oneToMany'] as $property => $options) {
                $name = explode('\\', str_replace($entityNamespace, '', $options['targetEntity']));
                $name = implode('', $name);

                $param = sprintf('$fk%s', ucfirst($name));

                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToMany (owning side)';
                $lines[] = '<space> * Add ' . $name;
                $lines[] = '<space> *';
                $lines[] = '<space> * @param ' . sprintf('\%s %s', $options['targetEntity'], $param);
                $lines[] = '<space> * @return ' . $entityName;
                $lines[] = '<space> */';
                $lines[] = '<space>public function add' . sprintf('%s(\%s %s)', sprintf('%s', ucfirst($property)), $options['targetEntity'], $param);
                $lines[] = '<space>{';
                $lines[] = '<space><space>' . sprintf('if (false === $this->%s->contains(%s)) {', $property, $param);
                $lines[] = '<space><space><space>' . sprintf('%s->set%s($this);', $param, ucfirst($options['mappedBy']));
                $lines[] = '<space><space><space>$this->' . sprintf('%s->add(%s);', $property, $param);
                $lines[] = '<space><space>}';
                $lines[] = '<space><space>';
                $lines[] = '<space><space>return $this;';
                $lines[] = '<space>}';
                $lines[] = '';

                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToMany (owning side)';
                $lines[] = '<space> * Remove ' . $name;
                $lines[] = '<space> *';
                $lines[] = '<space> * @param ' . sprintf('\%s %s', $options['targetEntity'], $param);
                $lines[] = '<space> */';
                $lines[] = '<space>public function remove' . sprintf('%s(\%s %s)', sprintf('%s', ucfirst($property)), $options['targetEntity'], $param);
                $lines[] = '<space>{';
                $lines[] = '<space><space>$this->' . sprintf('%s->removeElement(%s);', $property, $param);
                $lines[] = '<space>}';
                $lines[] = '';

                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToMany (owning side)';
                $lines[] = '<space> * Get ' . $property;
                $lines[] = '<space> *';
                $lines[] = '<space> * @return \Doctrine\Common\Collections\Collection|\\' . $options['targetEntity'];
                $lines[] = '<space> */';
                $lines[] = '<space>public function get' . sprintf('%s()', ucfirst($property));
                $lines[] = '<space>{';
                $lines[] = '<space><space>return $this->' . $property . ';';
                $lines[] = '<space>}';
                $lines[] = '';
            }
        }

        if (true === array_key_exists('manyToOne', $entity)) {
            foreach ($entity['manyToOne'] as $property => $options) {
                $lines[] = '<space>/**';
                $lines[] = '<space> * ManyToOne (inverse side)';
                $lines[] = '<space> * Set ' . $property;
                $lines[] = '<space> *';
                $lines[] = '<space> * @param ' . sprintf('\%s $%s', $options['targetEntity'], $property);
                $lines[] = '<space> * @return ' . $entityName;
                $lines[] = '<space> */';
                $lines[] = '<space>public function set' . sprintf('%s(\%s $%s)', sprintf('%s', ucfirst($property)), $options['targetEntity'], $property);
                $lines[] = '<space>{';

                foreach ($options['joinColumns'] as $setProperty => $getProperty) {
                    $setProperty = lcfirst(camelize($setProperty));
                    $getProperty = sprintf('get%s', camelize($getProperty['referencedColumnName']));

                    $lines[] = '<space><space>' . sprintf('$this->%s = $%s->%s();', $setProperty, $property, $getProperty);
                }

                $lines[] = '<space><space>' . sprintf('$this->%s = $%s;', $property, $property);
                $lines[] = '<space><space>';
                $lines[] = '<space><space>return $this;';
                $lines[] = '<space>}';
                $lines[] = '';

                $lines[] = '<space>/**';
                $lines[] = '<space> * ManyToOne (inverse side)';
                $lines[] = '<space> * Get ' . $property;
                $lines[] = '<space> *';
                $lines[] = '<space> * @return \\' . $options['targetEntity'];
                $lines[] = '<space> */';
                $lines[] = '<space>public function get' . sprintf('%s()', sprintf('%s', ucfirst($property)));
                $lines[] = '<space>{';
                $lines[] = '<space><space>' . sprintf('return $this->%s;', $property);
                $lines[] = '<space>}';
                $lines[] = '';
            }
        }

        if (true === array_key_exists('oneToOneOwningSide', $entity)) {
            foreach ($entity['oneToOneOwningSide'] as $property => $options) {
                $name = explode('\\', str_replace($entityNamespace, '', $options['targetEntity']));
                $name = implode('', $name);

                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToOne (inverse side)';
                $lines[] = '<space> * Set ' . $name;
                $lines[] = '<space> *';
                $lines[] = '<space> * @param ' . sprintf('\%s $%s', $options['targetEntity'], $property);
                $lines[] = '<space> * @return ' . $entityName;
                $lines[] = '<space> */';
                $lines[] = '<space>public function set' . sprintf('%s(\%s $%s)', ucfirst($property), $options['targetEntity'], $property);
                $lines[] = '<space>{';
                $lines[] = '<space><space>' . sprintf('$%s->set%s($this);', $property, ucfirst($options['mappedBy']));
                $lines[] = '<space><space>$this->' . sprintf('%s = $%s;', $property, $property);
                $lines[] = '<space><space>return $this;';
                $lines[] = '<space>}';
                $lines[] = '';

                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToOne (inverse side)';
                $lines[] = '<space> * Get ' . $property;
                $lines[] = '<space> *';
                $lines[] = '<space> * @return \\' . $options['targetEntity'];
                $lines[] = '<space> */';
                $lines[] = '<space>public function get' . sprintf('%s()', sprintf('%s', ucfirst($property)));
                $lines[] = '<space>{';
                $lines[] = '<space><space>' . sprintf('return $this->%s;', $property);
                $lines[] = '<space>}';
                $lines[] = '';
            }
        }

        if (true === array_key_exists('oneToOneInverseSide', $entity)) {
            foreach ($entity['oneToOneInverseSide'] as $property => $options) {
                $name = explode('\\', str_replace($entityNamespace, '', $options['targetEntity']));
                $name = implode('', $name);

                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToOne (owning side)';
                $lines[] = '<space> * Set ' . $name;
                $lines[] = '<space> *';
                $lines[] = '<space> * @param ' . sprintf('\%s $%s', $options['targetEntity'], $property);
                $lines[] = '<space> * @return ' . $entityName;
                $lines[] = '<space> */';
                $lines[] = '<space>public function set' . sprintf('%s(\%s $%s)', ucfirst($property), $options['targetEntity'], $property);
                $lines[] = '<space>{';

                foreach ($options['joinColumns'] as $setProperty => $getProperty) {
                    $setProperty = lcfirst(camelize($setProperty));
                    $getProperty = sprintf('get%s', camelize($getProperty['referencedColumnName']));

                    $lines[] = '<space><space>' . sprintf('$this->%s = $%s->%s();', $setProperty, $property, $getProperty);
                }

                $lines[] = '<space><space>$this->' . sprintf('%s = $%s;', $property, $property);
                $lines[] = '<space><space>return $this;';
                $lines[] = '<space>}';
                $lines[] = '';

                $lines[] = '<space>/**';
                $lines[] = '<space> * OneToOne (owning side)';
                $lines[] = '<space> * Get ' . $property;
                $lines[] = '<space> *';
                $lines[] = '<space> * @return \\' . $options['targetEntity'];
                $lines[] = '<space> */';
                $lines[] = '<space>public function get' . sprintf('%s()', sprintf('%s', ucfirst($property)));
                $lines[] = '<space>{';
                $lines[] = '<space><space>' . sprintf('return $this->%s;', $property);
                $lines[] = '<space>}';
                $lines[] = '';
            }
        }

        return implode("\n", $lines);
    };

    $getLifecycle = function($entity) use ($table, $getColumn) {
        $sequences = [];

        if (1 === count($entity['id'])) {
            return;
        }

        foreach ($entity['id'] as $property => $options) {
            if (null !== $sequence = $getColumn($property, $options)['sequence']) {
                $sequences[$property] = $sequence;
            }
        }

        if (0 === count($sequences)) {
            return;
        }

        $lines = [];
        $lines[] = '';
        $lines[] = '<space>/**';
        $lines[] = '<space> * PrePersist';
        $lines[] = '<space> * @param \Doctrine\Common\Persistence\Event\LifecycleEventArgs $args';
        $lines[] = '<space> */';
        $lines[] = '<space>public function generatePkSequence(\Doctrine\Common\Persistence\Event\LifecycleEventArgs $args)';
        $lines[] = '<space>{';

        foreach ($sequences as $property => $sequence) {
            $lines[] = '<space><space>$this->' . sprintf('%s = (new \Doctrine\ORM\Id\SequenceGenerator(\'%s\', 1))->generate($args->getObjectManager(), $this);', $property, $sequence);
        }

        $lines[] = '<space>}';

        return implode("\n", $lines);
    };


    $placeHolders = array(
        '<namespace>',
        '<entityAnnotation>',
        '<entityClassName>',
        '<entityBody>',
        '<constructor>',
        '<settersAndGetters>',
        '<lifecycle>',
    );

    $namespace = array_keys($entity)[0];
    $namespace = explode('\\', $namespace);

    $entityName = array_pop($namespace);
    $namespace = sprintf('namespace %s;', implode('\\', $namespace));

    $entity = reset($entity);

    $replacements = array(
        $namespace . "\n",
        $getEntityDocBlock($entityName),
        'class ' . $entityName,
        $getEntityProperties($entity),
        $getEntityConstructor($entity),
        trim($getEntitySettersAndGetters($entity, $entityName, $entityNamespace), "\n"),
        $getLifecycle($entity),
    );

    return str_replace('<space>', '    ', str_replace($placeHolders, $replacements, $classTemplate)) . "\n";
}
